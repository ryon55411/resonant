﻿using Dalamud.Game.Command;
using Dalamud.IoC;
using Dalamud.Plugin;
using Dalamud.Plugin.Services;

namespace Resonant
{
    public sealed class Plugin : IDalamudPlugin
    {
        public string Name => "Resonant";

        private DalamudPluginInterface DalamudInterface { get; }
        private ICommandManager CommandManager { get; }
        private ConfigurationManager ConfigManager { get; }

        private ConfigurationUI ConfigUI { get; }
        private DebugUI DebugUI { get; }
        private ResonantCore ResonantCore { get; }

        public Plugin(
            [RequiredVersion("1.0")] DalamudPluginInterface dalamudInterface,
            [RequiredVersion("1.0")] ICommandManager commandManager,
            IClientState clientState,
            IGameGui gameGui,
            IDataManager dataManager
        )
        {
            DalamudInterface = dalamudInterface;
            CommandManager = commandManager;

            ConfigManager = new ConfigurationManager(DalamudInterface);

            ConfigUI = new ConfigurationUI(ConfigManager, dataManager);
            DebugUI = new DebugUI(ConfigManager, clientState);

            ResonantCore = new ResonantCore(ConfigManager, clientState, gameGui, dataManager);

            Initialize();
        }

        internal void Initialize()
        {
            CommandManager.AddHandler("/resonant", new CommandInfo(HandleSlashCommand)
            {
                HelpMessage = "Toggle configuration",
            });

            DalamudInterface.UiBuilder.Draw += Draw;

            DalamudInterface.UiBuilder.OpenConfigUi += () =>
            {
                ConfigManager.ConfigUIVisible = true;
            };

            // hack: show config by default if debug is enabled
            if (ConfigManager.DebugUIVisible)
            {
                ConfigManager.ConfigUIVisible = true;
            }
        }

        internal void Draw()
        {
            ConfigUI.Draw();
            ResonantCore.Draw();
            DebugUI.Draw();
        }

        internal void HandleSlashCommand(string command, string args)
        {
            ConfigManager.ConfigUIVisible = !ConfigManager.ConfigUIVisible;
        }

        public void Dispose()
        {
            CommandManager.RemoveHandler("/resonant");
        }
    }
}
