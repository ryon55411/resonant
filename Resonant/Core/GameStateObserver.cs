using Dalamud.Plugin.Services;
using System;

namespace Resonant
{
    internal class GameStateObserver
    {
        IClientState ClientState { get; }
        IDataManager DataManager { get; }

        // todo: use enum value from Lumina instead of string abbreviation
        string? CurrentJobAbbrev;

        public event EventHandler<string> JobChangedEvent;

        // todo: figure out why c# is giving a warning about non-nullable event
        internal GameStateObserver(IClientState clientState, IDataManager dataManager)
        {
            ClientState = clientState;
            DataManager = dataManager;

            CurrentJobAbbrev = CurrentJob();
        }

        internal void Observe()
        {
            var observedClassJob = CurrentJob();

            if (observedClassJob != CurrentJobAbbrev)
            {
                CurrentJobAbbrev = observedClassJob;
                JobChangedEvent?.Invoke(this, observedClassJob);
            }
        }

        private string CurrentJob()
        {
            return ClientState.LocalPlayer?.ClassJob.GameData?.Abbreviation ?? "UNKNOWN";
        }
    }
}